# Front-End test

This project is a coding test that is part of our recruitment process for front-end developers.

To complete this step of the recruitment process you should **fork this repository and deliver a link to your solution**. Don't forget to make your forked project a public repository.

Within the project you can find some assets, like a font to be imported, some game assets (SVGs), wireframes and a description of each section of the test.

## Candidate notes:
We want you to implement a standard web app, following the [Layout Specification](#layout-specifications) section and the wireframes provided on the `Design_Specs` folder.

We also provide an assets folder, where you can find all you need to accomplish the layout execution, including a font and some images. As you should have noticed, there is also a "Color palette" file on the Design specs folder.

Feel free to edit the layout and decorate it as you want. Animations are welcome too. The only requirements for the included assets pack are the usage of the custom font and the gaming assets you may need.

**Usage of frameworks like Angular, React or VueJS are also up to you.
Don't forget we all have phones and we may want to play your awesome game during our coffee break.**

Please deliver this test, even if it's incomplete.


## Layout Specifications

### Section 1
Make any change you find appropriate to make the existing website responsive

#### Details:
- Follow the design specifications included on the  `Design_Specs` folder
___
### Section 2
Implement the tic tac toe section.

#### Details:
- Follow the design specifications included on the  `Design_Specs` folder
- This section **must** be the size of the viewport

- Basic section requirements:
  - Game board
  - Implement a working tic-tac-toe game
  - When one of the players wins, the board should be replaced with a victory feedback, showing that players name
  - Each player's turn should be signaled with a background change to the player's name (assume player 1 is always the one that starts the game)

- Basic Game mechanisms:
  - Detect victories
  - Highlight victory line and tokens
  - Time counter per game

(feel free to add more features if you want)
___
### Section 3
Implement the statistics/history section

#### Details:
- Follow the design specifications included on the  `Design_Specs` folder
- This section **must** be the size of the viewport
- Number of games played
- Count players wins
- Save victory history
- Percentage of victories for each player
- History of game winners
- Total play time
___

## Bonus Features
On Section 3, you can earn some extra respect points by either:

#### Option 1:
##### Dynamic grid tic-tac-toe
Select box to change game grid (3x3, 6x6, 9x9)

#### Option 2:
##### 4-in-a-row game
- Filter on top of game section (3) to toggle game with this being 4-in-a-row.
- Both games should be functional and respect the requirements stated above.
